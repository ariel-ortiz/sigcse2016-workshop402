from http.client import HTTPConnection
from sys import stderr
from json import loads

URL = 'http://localhost:8080/quotations'

conn = HTTPConnection("localhost:8080")
try:
    conn.request("GET", URL)
    res = conn.getresponse()
    body = res.read().decode('utf-8')
    if res.status != 200:
        raise Exception(str(res.status) + ' '
            + res.reason + '. ' + body)
    for q in loads(body):
        print('{0} - {1}: {2}'.format(q['id'], q['author'], q['prelude']))
except Exception as err:
    print('ERROR: ' + str(err), file=stderr)