'use strict';

const request = require('request');
const URL = 'http://localhost:8080/quotations';

request(URL, (err, response, body) => {
  if (err || response.statusCode !== 200) {
    console.error('ERROR: ' + (err || body));
  } else {
    let result = JSON.parse(body);
    result.forEach((q) => {
      console.log('%d - %s: ', q.id, q.author, q.prelude);
    });
  }
});
