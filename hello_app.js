'use strict'; 

const express = require('express'); 
const app = new express(); 

app.get('/hello', (req, res) => { 
  let who = req.query.who || 'Anonymous'
  res.type('text').send('Hello ' + who + '!\n'); 
});

app.listen(process.env.PORT, () => { 
  console.log('Server running as: ' + process.env.C9_HOSTNAME); 
});
